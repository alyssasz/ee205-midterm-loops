///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Midterm - Loops
///
/// @file    main.c
/// @version 1.0
///
/// Sums up values in arrays
///
/// Your task is to print out the sums of the 3 arrays...
///
/// The three arrays are held in a structure.  Consult numbers.h for the details.
///
/// For array1[], you'll iterate over using a for() loop.  The correct sum for 
/// array1[] is:  48723737032
/// 
/// For array2[], you'll iterate over it using a while()... loop.
///
/// For array3[], you'll iterate over it using a do ... while() loop.
///
/// Sample output:  
/// $ ./main
/// 11111111111
/// 22222222222
/// 33333333333
/// $
///
/// @brief Midterm - Loops - EE 205 - Spr 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include "numbers.h"

int main() {
   
   int sum1;
   int sum2;
   int sum3; 
   
   
   // Sum array1[] with a for() loop and print the result
   for (int i = 0; i <= 100; i++) {
      sum1+=array1[i];
      printf("%ld", sum1);


   }

   // Sum array2[] with a while() { } loop and print the result
   int j = 0; 
   while (j <= 100) {
      sum2+=array2[j];
      printf("%ld", sum2);

   }


   // Sum array3[] with a do { } while() loop and print the result
   int k = 0;
   do {
      k++;
      sum3+=array3[k];
      printf("%ld", sum3);

   } while (k <= 100); {

   }


	return 0;
}

